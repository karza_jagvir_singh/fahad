from cache.linked_list import DoublyLinkedList


class LruCache(object):
	"""
		LruCache class supports "get" and "set" operations in O(1) time complexity
		The type of "key" should be python hashable object and "value" can be any python object
		
		The class holds a hash map and a doubly linked list to implement the get and set features

		Key part of hash map is the "key" of our cache data and value is tuple containing two elements
		1. Pointer to node in linked list
		2. Cached Data
		
		for eg. 
		{
			"123": (pointer_to_node_containing_123, "data 123"),
			"456": (pointer_to_node_containing_456, "data 456"),
		}

		With the help of pointer to each node of linked list kept in hash map, deletion of any node can be acheived in O(1)

		In the "get" operation when hit is found, the rank of this key has to be increased, to acheive this in constant time, following procedure is followed
		1. With the help of Hash map find the pointer to this node
		2. Delete this node from Linked List
		3. Add this node at the tail of Linked List
		
		Two extra pointers are kept with Linked list, head and tail.
		- head pointer helps in selecting the node to be deleted from the cache when cache is full
		- tail pointer helps in adding the node in the cache with highest rank
	"""
	def __init__(self, size):
		self._size = size # private variable to keep cache size
		self._doubly_linked_list = DoublyLinkedList()
		self._current_occupied_size = 0 
		# This will hold the cache key as key part of hash map and data will be tuple (pointer_to_node, cached_data)
		self._key_node_map = {}

	def __repr__(self):
		"""
			Represents the Cache as Linked list
		"""
		return repr(self._doubly_linked_list)

	def delete(self, key):
		"""
			Returns True if deleted successfully else False
		"""
		if not self._key_node_map.get(key):
			return False

		node, data = self._key_node_map[key]
		self._doubly_linked_list.remove_node(node)
		del self._key_node_map[key]
		self._current_occupied_size -= 1

		return True

	def get(self, key):
		"""
			returns None if data is not found in cache
			returns data if found in cache
			
			If data is found in the cache, then rank of it is increased
		"""

		if not self._key_node_map.get(key):
			return None

		node, data = self._key_node_map.get(key)

		self._doubly_linked_list.remove_node(node)
		new_node = self._doubly_linked_list.add_at_tail(node.data)
		self._key_node_map[key] = (new_node, data)

		return data
		
	def set(self, key, value):
		"""
			New node is added in the cache if space is empty
			Otherwise Least Recently Used key is removed to make space for the new data

			returns True on successful insertion
		"""
		if self._key_node_map.get(key):
			# invalidate the cache explicitly for key
			self.delete(key)

		if self._current_occupied_size >= self._size:
			removed_data_key = self._doubly_linked_list.remove_from_head()
			assert removed_data_key is not None
			del self._key_node_map[removed_data_key]
			self._current_occupied_size -= 1

		new_node = self._doubly_linked_list.add_at_tail(key)
		self._key_node_map[key] = (new_node, value)
		self._current_occupied_size += 1

		return True
