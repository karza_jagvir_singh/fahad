class Node(object):
	def __init__(self, data, next_node=None, prev_node=None):
		self.data = data
		self.next = next_node
		self.prev = prev_node

	def __repr__(self):
		return "<Node:" + repr(self.data) + ">"


class DoublyLinkedList(object):
	def __init__(self):
		self._head = None
		self._tail = None

	def add_at_tail(self, data):
		"""
			Add node in the Linked list
		"""
		node = Node(data)

		if self._head is None and self._tail is None:
			# When Linked list is empty
			self._head = self._tail = node
		else:
			# When Linked list has at least one element
			self._tail.next = node
			node.prev = self._tail
			self._tail = node

		return node


	def remove_node(self, node):
		"""
			Removes the given node from the linked list and returns data of the given node
		"""
		data = node.data
		if node.prev is None and node.next is None:
			# When "node" is the only node in Linked list
			self._head = self._tail = None
		elif node.prev is None:
			# When node to be deleted is head node
			self._head = node.next
			node.next.prev = None
		elif node.next is None:
			# When node to be deleted is tail node
			node.prev.next = None
			self._tail = node.prev
		else:
			node.prev.next = node.next
			node.next.prev = node.prev

		del node
		return data

	def remove_from_head(self):
		"""
			Removes the node from the head pointer, and returns the data of head pointer
		"""
		if self._head is None:
			# When Linked list is already Empty
			return None

		if self._head.next is None:
			# When Linked list has only 1 node
			node = self._head
			self._head = self._tail = None
			data = node.data
		else:
			node = self._head
			self._head.next.prev = None
			self._head = self._head.next
			data = node.data
		
		del node
		return data

	def traverse_forward(self):
		"""
			For testing purpose to check if the links are made correctly in the linked list
		"""
		elements = []
		pointer = self._head
		while pointer is not None:
			elements.append(pointer.data)
			pointer = pointer.next

		return elements
	
	def traverse_reverse(self):
		"""
			For testing purpose to check if the links are made correctly in the linked list
		"""
		elements = []
		pointer = self._tail
		while pointer is not None:
			elements.append(pointer.data)
			pointer = pointer.prev

		return elements


	def __repr__(self):
		"""
			Represents the Linked list for printing purpose
		"""
		if self._head is None:
			return "Empty"
		
		pointer = self._head
		elems = []
		while pointer is not None:
			elems.append(repr(pointer.data))
			pointer = pointer.next

		return "|" + " <-> ".join(elems) + "|"
