from cache.lru_cache import LruCache


if __name__ == "__main__":
	sqr = lambda x: x*x

	cache = LruCache(10)
	for element in [1, 2, 3, 4, 2, 5, 6, 5]:
		# print("Elem : ", element)
		data = cache.get(element)
		hit = None
		if data is None:
			data = sqr(element)
			cache.set(element, data)
			hit = False
		else:
			hit = True

		if hit:
			print("HIT", element, cache)
		else:
			print("MIS", element, cache)
