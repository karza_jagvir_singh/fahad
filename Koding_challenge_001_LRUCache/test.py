from cache.lru_cache import LruCache


def dummy_process_function(x):
	return x * x


def test():
	cache = LruCache(3)
	elements = [1, 2, 3, 4, 2, 5, 6, 5]

	assert cache.get(1) is None, "Cache should be empty"
	assert cache.set(2, dummy_process_function(2)) is True
	assert cache.get(2) == dummy_process_function(2)

	cache.set(3, dummy_process_function(3))
	cache.set(4, dummy_process_function(4))
	assert cache.get(1) is None, "1 should have been invalidated"

	assert cache.get(2) == dummy_process_function(2)
	cache.set(5, dummy_process_function(5))
	# cache.set(6, dummy_process_function(6))
	assert cache.get(2) == dummy_process_function(2), "2 should have been present in cache"
	cache.set(6, dummy_process_function(7))
	cache.set(6, dummy_process_function(8))

	cache.set(6, dummy_process_function(9))
	assert cache.get(6) == dummy_process_function(9), "value of cache should have been updated"

	cache.set(7, dummy_process_function(9))
	cache.set(8, dummy_process_function(9))
	assert cache.get(2) is None, "2 should have been invalidated"

if __name__ == "__main__":
	test()
